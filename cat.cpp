///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   dfsInorderReverse( topCat, 1 );
}

void CatEmpire::addCat( Cat* newCat ){
   if( topCat == nullptr ){
      //cout << "topcat is nullptr" << endl; 
      topCat = newCat;
      return; 
   }
   addCat( topCat, newCat );
}

void CatEmpire::addCat( Cat* atCat, Cat* newCat ){
   if( atCat->name > newCat->name ){
      if( atCat-> left == nullptr ){
         atCat->left = newCat;
         //cout << "Newcat is left of atcat" << endl;
      }else{
         addCat( atCat->left, newCat );
         //cout << "Left is take recall function" << endl;
      }   
   }

   if( atCat->name < newCat->name ){
      if( atCat-> right == nullptr ){
         atCat->right = newCat;
         //cout << "Newcat is right of atcat" << endl;
      }else{
         addCat( atCat->right, newCat );
         //cout << "Right is occupied recall function" << endl;
      }
   }
}

void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
   dfsInorder( topCat );
}

void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}
	dfsPreorder( topCat );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
      if( atCat == nullptr ) return;
      dfsInorderReverse( atCat-> right, depth+1 );

      cout << string( 6*(depth-1), ' ' ) << atCat->name;
      //Leaf Node
      if( atCat -> left == nullptr && atCat-> right == nullptr )
         cout << endl;
      //Both left/right child
      else if( atCat-> left != nullptr && atCat-> right != nullptr )
         cout << "<" << endl;
      //Node has left child only
      else if( atCat-> left != nullptr && atCat-> right == nullptr )
         cout << "\\" << endl;
      //Node has right child only
      else if( atCat-> left == nullptr && atCat-> right != nullptr )
         cout << "/" << endl;
      dfsInorderReverse( atCat-> left, depth+1 );
      return;
}

void CatEmpire::dfsInorder( Cat* atCat ) const {
      if( atCat == nullptr ) return;
      dfsInorder( atCat->left );
      cout << atCat->name << endl;
      dfsInorder( atCat->right );
}

void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr ) return;
   
   if( atCat-> left != nullptr && atCat->right != nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   else if( atCat-> left!= nullptr && atCat->right == nullptr ){
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   else if( atCat->left == nullptr && atCat->right != nullptr ){
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }
   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );
}

void CatEmpire::catGenerations() const {
   queue<Cat*> catQueue;
   catQueue.push( topCat );
   catQueue.push( nullptr );
   int generations = 1;
   cout << generations;
   getEnglishSuffix( generations );
   cout << " Generation" << endl;
   cout << "  ";
   while ( !catQueue.empty() ){
      Cat* cat = catQueue.front(); // dequeue cat
      catQueue.pop();
      if( cat == nullptr ){
         generations++;
         catQueue.push(nullptr);
         if(catQueue.front() == nullptr) break;
         else{ 
         cout << endl;
         cout << generations;
         getEnglishSuffix( generations );
         cout << " Generation" << endl;
         cout << "  " ; 
         continue;
         }
      }

      if( cat->left != nullptr )
         catQueue.push( cat->left );
      if( cat->right != nullptr )
         catQueue.push( cat->right);

      cout << cat->name << "  ";
   }
   cout << endl;
}

void CatEmpire::getEnglishSuffix ( int n ) const {
   if( n < 0 ) cout << "Invalid Negative Number" << endl;
   if( (n % 100 >= 10) && (n % 100 <= 20) ){
      cout << "th";
   }
   else if ( n % 10 == 1 )
      cout << "st";
   else if ( n % 10 == 2 )
      cout << "nd";
   else if ( n % 10 == 3 )
      cout << "rd";
   else 
      cout << "th";
}
