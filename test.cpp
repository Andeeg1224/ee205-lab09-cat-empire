///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// unit test file
///
/// @author Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   29_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include "cat.hpp"

using namespace std;

int main(){
   cout << "Hello World" << endl;
   CatEmpire empire;
   Cat::initNames();
   Cat* cat1 = Cat::makeCat();
   Cat* cat2 = Cat::makeCat();
   Cat* cat3 = Cat::makeCat();
   Cat* cat4 = Cat::makeCat();
   empire.addCat( cat1 );
   empire.addCat( cat2 );
   empire.addCat( cat3 );
   empire.addCat( cat4 );
}
